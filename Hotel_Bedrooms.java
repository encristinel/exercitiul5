

public class Hotel_Bedrooms extends Bedrooms implements MyInterface {

    private int dimension;
    private int height;
    private int weight;

    public Hotel_Bedrooms (int dimension, int height, int weight){

        this.dimension = dimension;
        this.height = height;
        this.weight = weight;

    }

    public int getDimension() {
        return dimension;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }


    String style;

    {
        style = "Headboard and siderails ";
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int CalculateSize(){

        return dimension + height + weight;
    }

    public String getStyle(){

        return "The style of this bed is " + style;
    }

}
